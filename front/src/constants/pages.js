const PAGES = {
  GET_PAGES: `${process.env.VUE_APP_ENDPOINT}/pages`,
  GET_HOME: `${process.env.VUE_APP_ENDPOINT}/pages/?slug=accueil&_embed&acf_format=standard`
}

export default PAGES
