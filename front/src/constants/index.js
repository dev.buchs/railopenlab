import COMMONS from './commons.js'
import PAGES from './pages.js'
import POSTS from './posts.js'

export default {
  COMMONS,
  PAGES,
  POSTS
}