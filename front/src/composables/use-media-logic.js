export function useMedia () {
  const getMedia = (id, arr) => {
    if (!id) return false
    return arr.find(arr => arr.id === id)
  }

  return {
    getMedia
  }
}