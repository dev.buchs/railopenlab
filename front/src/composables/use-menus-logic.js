export function useMenus () {
  const parseNavBarUrl = (url) => {
    const str = url.substr(process.env.NODE_ENV === 'development' ? 38 : 28, url.length - 1);
    return str.length ? str : '/';
  }

  return {
    parseNavBarUrl
  }
}