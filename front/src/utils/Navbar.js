import COMMONS from '@/constants/commons'
import axios from 'axios'

const getNavbar = async () => {
  const response = await axios.get(COMMONS.MENUS.GET_NAVBAR)
  return response.data
}


export default getNavbar