import COMMONS from '@/constants/commons'
import axios from 'axios'

const getFooter = async () => {
  const response = await axios.get(COMMONS.MENUS.GET_FOOTER)
  return response.data
}


export default getFooter