# RAIL OPEN LAB
## FRONT

### Run front app
```
npm run serve
```

### .env variables
- Copy `.env.default` to `.env` and replace `VUE_APP_URL_DEV` with your own dev URL
- If you have to had a new variable, don't forget to add `VUE_APP_` before variable name. Vue cli 3 loads only variables that start with `VUE_APP_`.



##BACK
