<?php
add_action('init', 'company_cpt');

function company_cpt() {
  $args = array(
    // Le nom au pluriel
    'name'                => _x( 'Partenaire', 'Post Type General Name'),
    // Le nom au singulier
    'singular_name'       => _x( 'Partenaire', 'Post Type Singular Name'),
    // Le libellé affiché dans le menu
    'menu_name'           => __( 'Partenaires'),
    // Les différents libellés de l'administration
    'all_items'           => __( 'Tous les partenaires'),
    'view_item'           => __( 'Voir les partenaires'),
    'add_new_item'        => __( 'Ajouter un nouveau partenaire'),
    'add_new'             => __( 'Ajouter'),
    'edit_item'           => __( 'Editer le partenaire'),
    'update_item'         => __( 'Modifier le partenaire'),
    'search_items'        => __( 'Rechercher un partenaire'),
    'not_found'           => __( 'Non trouvé'),
    'not_found_in_trash'  => __( 'Non trouvé dans la corbeille'),
    'label'               => __( 'Partenaires'),
    'description'         => __( 'Tous sur le partenaire'),
    'menu_icon'           => 'dashicons-store',
    // On définit les options disponibles dans l'éditeur de notre custom post type ( un titre, un auteur...)
    'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields', ),
    /* 
    * Différentes options supplémentaires
    */	
    'show_in_rest' => true, // Cette ligne permets d'acceder au custom post type de maniere native avec 'wp-json/wp/v2/realisations'
    'hierarchical'        => false,
    'public'              => true,
    'has_archive'         => true,
    'rewrite'			  => array( 'slug' => 'societes'),
  );

  // On enregistre notre custom post type qu'on nomme ici "serietv" et ses arguments
  register_post_type( 'societes', $args );

}