<?php

/**
 * Register a Année taxonomies, with REST API support
 *
 * Based on example at: https://codex.wordpress.org/Function_Reference/register_taxonomy
 */
add_action( 'init', 'post_annee');

function post_annee() {
  $labels = array(
    'name'              => _x( 'Années', 'taxonomy general name' ),
    'singular_name'     => _x( 'Année', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Années' ),
    'all_items'         => __( 'All Années' ),
    'parent_item'       => __( 'Parent Année' ),
    'parent_item_colon' => __( 'Parent Année:' ),
    'edit_item'         => __( 'Edit Année' ),
    'update_item'       => __( 'Update Année' ),
    'add_new_item'      => __( 'Add New Année' ),
    'new_item_name'     => __( 'New Année Name' ),
    'menu_name'         => __( 'Années' ),
  );

  $args = array(
    'hierarchical'          => true,
    'labels'                => $labels,
    'show_ui'               => true,
    'show_admin_column'     => true,
    'query_var'             => true,
    'rewrite'               => array( 'slug' => 'annees-post' ),
    'show_in_rest'          => true,
    'rest_base'             => 'annees-post',
    'rest_controller_class' => 'WP_REST_Terms_Controller', 
  );

  register_taxonomy( 'annees-post', array( 'post' ), $args );

}