<?php
add_action('init', 'testimony_cpt');

function testimony_cpt() {
  $labels = array(
    // Le nom au pluriel
    'name'                => _x( 'Témoignages', 'Post Type General Name'),
    // Le nom au singulier
    'singular_name'       => _x( 'Témoignage', 'Post Type Singular Name'),
    // Le libellé affiché dans le menu
    'menu_name'           => __( 'Témoignages'),
    // Les différents libellés de l'administration
    'all_items'           => __( 'Toutes les Témoignages'),
    'view_item'           => __( 'Voir les Témoignages'),
    'add_new_item'        => __( 'Ajouter une nouvelle Témoignage'),
    'add_new'             => __( 'Ajouter'),
    'edit_item'           => __( 'Editer la Témoignages'),
    'update_item'         => __( 'Modifier la Témoignages'),
    'search_items'        => __( 'Rechercher une Témoignage'),
    'not_found'           => __( 'Non trouvée'),
    'not_found_in_trash'  => __( 'Non trouvée dans la corbeille'),
  );

  // On peut définir ici d'autres options pour notre custom post type

  $args = array(
    'label'               => __( 'Témoignages'),
    'description'         => __( 'Tous sur Témoignages'),
    'labels'              => $labels,
    'menu_icon'           => 'dashicons-format-quote',
    // On définit les options disponibles dans l'éditeur de notre custom post type ( un titre, un auteur...)
    'supports'            => array( 'title', 'thumbnail', 'revisions', 'custom-fields' ),
    /* 
    * Différentes options supplémentaires
    */	
    'show_in_rest' => true, // Cette ligne permets d'acceder au custom post type de maniere native avec 'wp-json/wp/v2/realisations'
    'hierarchical'        => false,
    'public'              => true,
    'has_archive'         => true,
    'rewrite'			  => array( 'slug' => 'testimonies'),

  );

  // On enregistre notre custom post type qu'on nomme ici "serietv" et ses arguments
  register_post_type( 'testimony', $args );
}