<?php 
add_action( 'init', 'realisation_cpt' );

function realisation_cpt() {

  // On rentre les différentes dénominations de notre custom post type qui seront affichées dans l'administration
  $labels = array(
    // Le nom au pluriel
    'name'                => _x( 'Réalisations', 'Post Type General Name'),
    // Le nom au singulier
    'singular_name'       => _x( 'Réalisation', 'Post Type Singular Name'),
    // Le libellé affiché dans le menu
    'menu_name'           => __( 'Réalisations'),
    // Les différents libellés de l'administration
    'all_items'           => __( 'Toutes les réalisations'),
    'view_item'           => __( 'Voir les réalisations'),
    'add_new_item'        => __( 'Ajouter une nouvelle réalisation'),
    'add_new'             => __( 'Ajouter'),
    'edit_item'           => __( 'Editer la réalisations'),
    'update_item'         => __( 'Modifier la réalisations'),
    'search_items'        => __( 'Rechercher une réalisation'),
    'not_found'           => __( 'Non trouvée'),
    'not_found_in_trash'  => __( 'Non trouvée dans la corbeille'),
  );

  // On peut définir ici d'autres options pour notre custom post type

  $args = array(
    'label'               => __( 'Réalisations'),
    'description'         => __( 'Tous sur réalisations'),
    'labels'              => $labels,
    'menu_icon'           => 'dashicons-portfolio',
    // On définit les options disponibles dans l'éditeur de notre custom post type ( un titre, un auteur...)
    'supports'            => array( 'title', 'excerpt', 'thumbnail', 'revisions', 'custom-fields' ),
    /* 
    * Différentes options supplémentaires
    */	
    'show_in_rest' => true, // Cette ligne permets d'acceder au custom post type de maniere native avec 'wp-json/wp/v2/realisations'
    'hierarchical'        => false,
    'public'              => true,
    'has_archive'         => true,
    'rewrite'			  => array( 'slug' => 'realisations'),

  );

  // On enregistre notre custom post type qu'on nomme ici "serietv" et ses arguments
  register_post_type( 'realisations', $args );


}

/**
 * Register a Partenaire taxonomies, with REST API support
 *
 * Based on example at: https://codex.wordpress.org/Function_Reference/register_taxonomy
 */
add_action( 'init', 'realisation_partenaires');

function realisation_partenaires() {
  $labels = array(
    'name'              => _x( 'Partenaires', 'taxonomy general name' ),
    'singular_name'     => _x( 'Partenaire', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Partenaires' ),
    'all_items'         => __( 'All Partenaires' ),
    'parent_item'       => __( 'Parent Partenaire' ),
    'parent_item_colon' => __( 'Parent Partenaire:' ),
    'edit_item'         => __( 'Edit Partenaire' ),
    'update_item'       => __( 'Update Partenaire' ),
    'add_new_item'      => __( 'Add New Partenaire' ),
    'new_item_name'     => __( 'New Partenaire Name' ),
    'menu_name'         => __( 'Partenaires' ),
  );

  $args = array(
    'hierarchical'          => true,
    'labels'                => $labels,
    'show_ui'               => true,
    'show_admin_column'     => true,
    'query_var'             => true,
    'rewrite'               => array( 'slug' => 'categories-real' ),
    'show_in_rest'          => true,
    'rest_base'             => 'partenaires-real',
    'rest_controller_class' => 'WP_REST_Terms_Controller', 
  );

  register_taxonomy( 'partenaires-real', array( 'realisations' ), $args );

}

/**
 * Register a Thematiques taxonomies, with REST API support
 *
 * Based on example at: https://codex.wordpress.org/Function_Reference/register_taxonomy
 */
add_action( 'init', 'realisation_thematiques');

function realisation_thematiques() {
  $labels = array(
    'name'              => _x( 'Thematiques', 'taxonomy general name' ),
    'singular_name'     => _x( 'Thematique', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Thematiques' ),
    'all_items'         => __( 'All Thematiques' ),
    'parent_item'       => __( 'Parent Thematique' ),
    'parent_item_colon' => __( 'Parent Thematique:' ),
    'edit_item'         => __( 'Edit Thematique' ),
    'update_item'       => __( 'Update Thematique' ),
    'add_new_item'      => __( 'Add New Thematique' ),
    'new_item_name'     => __( 'New Thematique Name' ),
    'menu_name'         => __( 'Thematiques' ),
  );

  $args = array(
    'hierarchical'          => true,
    'labels'                => $labels,
    'show_ui'               => true,
    'show_admin_column'     => true,
    'query_var'             => true,
    'rewrite'               => array( 'slug' => 'thematiques-real' ),
    'show_in_rest'          => true,
    'rest_base'             => 'thematiques-real',
    'rest_controller_class' => 'WP_REST_Terms_Controller', 
  );

  register_taxonomy( 'thematiques-real', array( 'realisations' ), $args );

}

/**
 * Register a Année taxonomies, with REST API support
 *
 * Based on example at: https://codex.wordpress.org/Function_Reference/register_taxonomy
 */
add_action( 'init', 'realisation_annee');

function realisation_annee() {
  $labels = array(
    'name'              => _x( 'Années', 'taxonomy general name' ),
    'singular_name'     => _x( 'Année', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Années' ),
    'all_items'         => __( 'All Années' ),
    'parent_item'       => __( 'Parent Année' ),
    'parent_item_colon' => __( 'Parent Année:' ),
    'edit_item'         => __( 'Edit Année' ),
    'update_item'       => __( 'Update Année' ),
    'add_new_item'      => __( 'Add New Année' ),
    'new_item_name'     => __( 'New Année Name' ),
    'menu_name'         => __( 'Années' ),
  );

  $args = array(
    'hierarchical'          => true,
    'labels'                => $labels,
    'show_ui'               => true,
    'show_admin_column'     => true,
    'query_var'             => true,
    'rewrite'               => array( 'slug' => 'annees-real' ),
    'show_in_rest'          => true,
    'rest_base'             => 'annees-real',
    'rest_controller_class' => 'WP_REST_Terms_Controller', 
  );

  register_taxonomy( 'annees-real', array( 'realisations' ), $args );

}

add_action( 'init', 'realisation_tags');

function realisation_tags() {
  $labels = array(
    'name'              => _x( 'Tags', 'taxonomy general name' ),
    'singular_name'     => _x( 'Tag', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Tags' ),
    'all_items'         => __( 'All Tags' ),
    'parent_item'       => __( 'Parent Tag' ),
    'parent_item_colon' => __( 'Parent Tag:' ),
    'edit_item'         => __( 'Edit Tag' ),
    'update_item'       => __( 'Update Tag' ),
    'add_new_item'      => __( 'Add New Tag' ),
    'new_item_name'     => __( 'New Tag Name' ),
    'menu_name'         => __( 'Tags' ),
  );

  $args = array(
    'hierarchical'          => true,
    'labels'                => $labels,
    'show_ui'               => true,
    'show_admin_column'     => true,
    'query_var'             => true,
    'rewrite'               => array( 'slug' => 'tags-real' ),
    'show_in_rest'          => true,
    'rest_base'             => 'tags-real',
    'rest_controller_class' => 'WP_REST_Terms_Controller', 
  );

  register_taxonomy( 'tags-real', array( 'realisations' ), $args );

}

