<?php
add_action('init', 'label_cpt');

function label_cpt() {
  $args = array(
    // Le nom au pluriel
    'name'                => _x( 'Label', 'Post Type General Name'),
    // Le nom au singulier
    'singular_name'       => _x( 'Label', 'Post Type Singular Name'),
    // Le libellé affiché dans le menu
    'menu_name'           => __( 'Labels'),
    // Les différents libellés de l'administration
    'all_items'           => __( 'Toutes les Labels'),
    'view_item'           => __( 'Voir les Labels'),
    'add_new_item'        => __( 'Ajouter un nouveau label'),
    'add_new'             => __( 'Ajouter'),
    'edit_item'           => __( 'Editer le Label'),
    'update_item'         => __( 'Modifier le Label'),
    'search_items'        => __( 'Rechercher un Label'),
    'not_found'           => __( 'Non trouvé'),
    'not_found_in_trash'  => __( 'Non trouvé dans la corbeille'),
    'label'               => __( 'Labels'),
    'description'         => __( 'Tous sur label'),
    'menu_icon'           => 'dashicons-editor-paragraph',
    // On définit les options disponibles dans l'éditeur de notre custom post type ( un titre, un auteur...)
    'supports'            => array( 'title', 'thumbnail', 'revisions', 'custom-fields'),
    /* 
    * Différentes options supplémentaires
    */	
    'show_in_rest' => true, // Cette ligne permets d'acceder au custom post type de maniere native avec 'wp-json/wp/v2/realisations'
    'hierarchical'        => false,
    'public'              => true,
    'has_archive'         => true,
    'rewrite'			  => array( 'slug' => 'labels'),
  );

  // On enregistre notre custom post type qu'on nomme ici "serietv" et ses arguments
  register_post_type( 'labels', $args );

}

add_action( 'init', 'label_categories');

function label_categories() {
  $labels = array(
    'name'              => _x( 'Catégories', 'taxonomy general name' ),
    'singular_name'     => _x( 'Catégorie', 'taxonomy singular name' ),
    'search_items'      => __( 'Rechercher catégories' ),
    'all_items'         => __( 'Toutes les catégories' ),
    'parent_item'       => __( 'Catégorie parente' ),
    'parent_item_colon' => __( 'Catégorie parente:' ),
    'edit_item'         => __( 'Modifier la catégorie' ),
    'update_item'       => __( 'Mettre à jour la catégorie' ),
    'add_new_item'      => __( 'Ajouter une nouvelle catégorie' ),
    'new_item_name'     => __( 'Nom de la catégorie' ),
    'menu_name'         => __( 'Catégories' ),
  );

  $args = array(
    'hierarchical'          => true,
    'labels'                => $labels,
    'show_ui'               => true,
    'show_admin_column'     => true,
    'query_var'             => true,
    'rewrite'               => array( 'slug' => 'labels_categories' ),
    'show_in_rest'          => true,
    'rest_base'             => 'labels_categories',
    'rest_controller_class' => 'WP_REST_Terms_Controller', 
  );

  register_taxonomy( 'labels_categories', array( 'labels' ), $args );

}
