<?php
function get_menu() {
	return wp_get_nav_menu_items('primary');
}

add_action( 'rest_api_init', function () {
		register_rest_route( 'wp/v2/', '/menu', array(
			'methods' => 'GET',
			'callback' => 'get_menu',
	));
});


function get_menu_footer() {
	return wp_get_nav_menu_items('footer');
}

add_action( 'rest_api_init', function () {
		register_rest_route( 'wp/v2/', '/footer', array(
		'methods' => 'GET',
		'callback' => 'get_menu_footer',
	));
});
