<?php
/**
 * Adds a homepage endpoint for generell homepage settings in the
 * Front-end
 */
add_action('rest_api_init', 'homepage_route');
 
function homepage_route() {
	register_rest_route('wp', '/v2/homepage', array(
			'methods'  => 'GET',
			'callback' => 'get_homepage'
	));
}
 
function get_homepage( $object ) {
 
	$request  = new WP_REST_Request('GET', '/wp/v2/posts');

	$homepage_id = get_option('page_on_front');
	if ( $homepage_id ) {
		$request  = new WP_REST_Request('GET', '/wp/v2/pages/' . $homepage_id);
	}

	$response = rest_do_request($request);
	if ($response->is_error()) {
		return new WP_Error('request_error', __('Request Error'), array('status' => 500 ));
	}

	$embed = $object->get_param('_embed') !== NULL;
	$data = rest_get_server()->response_to_data( $response, $embed);

	return $data;
}
