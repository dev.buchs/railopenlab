<?php

/**
 * Rail Open Lab functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Rail_Open_Lab
 * @since Rail Open Lab 1.0
 */

/**
 * SETUP WORDPRESS BACKEND
 */
 require('setup.php');

/**
 * MENU ITEMS
 * @routes: '/wp/v2/menu'
 */
require('modules/navbar.php');

/**
 * HOME PAGE
 * @routes: '/wp/v2/homepage'
 */
require('modules/homepage.php');


/**
 * CUSTOM POST: Réalisations
 * @routes: '/wp/v2/realisations'
 */
require('modules/realisation.php');

/**
 * Societe
 * @routes: '/wp/v2/societes'
 */
require('modules/company.php');

/**
 * Post
 * @routes '/wp/v2/posts
 */
require('modules/posts.php');

/**
 * Label
 * @routes '/wp/v2/labels
 */
require('modules/label.php');

/**
 * Testimonies
 * @routes
 */
require('modules/testimony.php');
?>
