<?php 

// ADD POST THUMBNAIL ON PAGE EDIT/CREATE
add_theme_support( 'post-thumbnails' );


// ADD MENU EDITOR ON WORDPRESS THEME
function railopenlab_theme_setup() {
    register_nav_menus( array( 
      'primary' => 'Primary menu', 
      'secondary' => 'Secondary menu' 
    ));
}
  
add_action( 'after_setup_theme', 'railopenlab_theme_setup' );


// REMOVE COMMENTS SECTION

add_action('admin_init', function () {
  // Redirect any user trying to access comments page
  global $pagenow;
   
  if ($pagenow === 'edit-comments.php') {
      wp_safe_redirect(admin_url());
      exit;
  }

  // Remove comments metabox from dashboard
  remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');

  // Disable support for comments and trackbacks in post types
  foreach (get_post_types() as $post_type) {
      if (post_type_supports($post_type, 'comments')) {
          remove_post_type_support($post_type, 'comments');
          remove_post_type_support($post_type, 'trackbacks');
      }
  }
});

// Close comments on the front-end
add_filter('comments_open', '__return_false', 20, 2);
add_filter('pings_open', '__return_false', 20, 2);

// Hide existing comments
add_filter('comments_array', '__return_empty_array', 10, 2);

// Remove comments page in menu
add_action('admin_menu', function () {
  remove_menu_page('edit-comments.php');
});

// Remove comments links from admin bar
add_action('init', function () {
  if (is_admin_bar_showing()) {
      remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
  }
});

// add_filter( 'use_block_editor_for_post', '__return_false' );

function remove_editor() {
  remove_post_type_support('page', 'editor');
  remove_post_type_support('post', 'editor');
  remove_post_type_support('page', 'revisions');
  remove_post_type_support('page', 'author');
}
add_action('admin_init', 'remove_editor');